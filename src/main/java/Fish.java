/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Fish extends AquaticAnimal{
    private String name;

    public Fish(String name) {
        super(name);
    }
    

    @Override
    public void swim() {
        System.out.println("Fish: "+name+" swim");
    }

   @Override
    public void sleep() {
        System.out.println("Fish: "+name+" sleep");
    }

    @Override
    public void eat() {
        System.out.println("Fish: "+name+" eat");
    }

    @Override
    public void walk() {
        System.out.println("Fish: "+name+" walk");
    }
    
    @Override
    public void speak() {
        System.out.println("Fish: "+name+" speak");
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class TestAnimal {
    public static void main(String[] args){
        Human h1=new Human("Dang");
        Cat c1=new Cat("Som");
        Dog d1=new Dog("Dum");
        Snake s1=new Snake("Nak");
        Crocodile cr1=new Crocodile("Here");
        Fish f1=new Fish("Yut");
        Crab ca1=new Crab("Poo");
        Bat b1=new Bat("Corona");
        Bird br1=new Bird("Nok");
        h1.eat();
        h1.walk();
        c1.run();
        d1.run();
        s1.crawl();
        cr1.crawl();
        f1.swim();
        ca1.swim();
        b1.fly();
        br1.fly();
        
    }
}

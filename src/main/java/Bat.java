/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Bat extends Poultry{
    private String name;

    public Bat(String name) {
        super(name, 2);
    }
    

    @Override
    public void fly() {
        System.out.println("Bat: "+name+" fly");
    }

    @Override
    public void sleep() {
        System.out.println("Bat: "+name+" sleep");
    }

    @Override
    public void eat() {
        System.out.println("Bat: "+name+" eat");
    }

    @Override
    public void walk() {
        System.out.println("Bat: "+name+" walk");
    }
    
    @Override
    public void speak() {
        System.out.println("Bat: "+name+" speak");
    }
    
}

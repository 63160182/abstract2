/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Crab extends AquaticAnimal{
    private String name;

    public Crab(String name) {
        super(name);
    }

    @Override
    public void swim() {
        System.out.println("Crab: "+name+" swim");
    }

   @Override
    public void sleep() {
        System.out.println("Crab: "+name+" sleep");
    }

    @Override
    public void eat() {
        System.out.println("Crab: "+name+" eat");
    }

    @Override
    public void walk() {
        System.out.println("Crab: "+name+" walk");
    }
    
    @Override
    public void speak() {
        System.out.println("Crab: "+name+" speak");
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Crocodile extends Reptile{
    private String name;

    public Crocodile(String name) {
        super(name,4);
    }
    
    @Override
    public void crawl() {
        System.out.println("Crocodile: "+name+" sleep");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile: "+name+" sleep");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile: "+name+" eat");
    }

    @Override
    public void walk() {
        System.out.println("Crocodile: "+name+" walk");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile: "+name+" speak");
    }
    
}

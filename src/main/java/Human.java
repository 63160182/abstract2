/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Human extends LandAnimal{
    private String nickname;
    
    public Human(String nickname) {
        super("Human", 2);
    }
    
    

    @Override
    public void run() {
        System.out.println("Human: "+nickname+" run");
    }

    @Override
    public void sleep() {
        System.out.println("Human: "+nickname+" sleep");
    }

    @Override
    public void eat() {
        System.out.println("Human: "+nickname+" eat");
    }

    @Override
    public void walk() {
        System.out.println("Human: "+nickname+" walk");
    }

    @Override
    public void speak() {
        System.out.println("Human: "+nickname+" speak");
    }
    
}
